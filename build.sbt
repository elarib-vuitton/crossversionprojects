
val scala12 = "2.12.15"
val scala11 = "2.11.10"

lazy val core = Project("core", file("src/core")).settings(
  scalaVersion := scala11,
  crossScalaVersions := Seq(scala11, scala12)
)

lazy val microService = Project("microservice", file("src/microservice"))
  .settings(
    scalaVersion := scala12
  )
  .dependsOn(core)

lazy val sparkJob = Project("sparkjob", file("src/sparkjob"))
  .settings(
    scalaVersion := scala11
  )
  .dependsOn(core)